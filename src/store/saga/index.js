import {takeEvery} from 'redux-saga/effects';
import types from '../actionsTypes';
import {getCityWeatherSaga} from './sagas';

export function* watchSaga() {
  yield takeEvery(types.GET_CITY_WEATHER, getCityWeatherSaga);

}
