import {call, put} from 'redux-saga/effects';
import {
    setLoading
} from '../actions/uIStateActions';
import {getCityWeatherApi} from '../../api';
import {setRealTemperature} from '../actions/weatherActions';

function parseData(data) {

    return Math.round(data.main.temp);
}

export function* getCityWeatherSaga(data) {
    console.log(data);
    try {
        yield put(setLoading(true));
        // yield put(killSvg());
        const response = yield call(getCityWeatherApi, data.payload);
        console.log(response);

        if (response.status === 200 && response.data) {
            const temperature = parseData(response.data);
            console.log(temperature);
            yield put(setRealTemperature(temperature));
        }

        yield put(setLoading(false));

    } catch (error) {
        console.log(error);
        yield put(setLoading(false));
    }
}

