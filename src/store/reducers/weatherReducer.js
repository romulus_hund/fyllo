import actionsTypes from '../actionsTypes';
import createReducer from './createReducer';

const initialState = {
    selectedCity: null,
    selectedTemperature: '',
    realTemperature: null,
    results: []
};

const weatherReducer = createReducer(initialState, {

    [actionsTypes.SET_CITY]: (state, {payload}) => {
        return {
            ...state,
            selectedCity: payload
        };
    },

    [actionsTypes.SET_TEMPERATURE]: (state, {payload}) => {
        return {
            ...state,
            selectedTemperature: payload
        };
    },

    [actionsTypes.SET_REAL_TEMPERATURE]: (state, {payload}) => {
        return {
            ...state,
            realTemperature: payload
        };
    },

    [actionsTypes.SET_RESULT]: (state, {payload}) => {
        return {
            ...state,
            results: [...state.results, payload]
        };
    },

});

export default weatherReducer;
