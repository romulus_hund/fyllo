import { combineReducers } from 'redux';
import weatherReducer from './weatherReducer';
import UIStateReducer from './uIStateReducer';


export default combineReducers({
  weatherReducer,
  UIStateReducer,
});
