
export const isLoading = state => state.UIStateReducer?.isLoading;

export const getSelectedCity = state => state.weatherReducer?.selectedCity;
export const getSelectedTemperature = state => state.weatherReducer?.selectedTemperature;
export const getRealTemperature = state => state.weatherReducer?.realTemperature;
export const getResults = state => state.weatherReducer?.results;

