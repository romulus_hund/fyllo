import types from '../actionsTypes';

export const getCityWeather = data => {
  return{
    type: types.GET_CITY_WEATHER,
    payload: data
  };
};

export const setCity = data => {
  return{
    type: types.SET_CITY,
    payload: data
  };
};

export const setTemperature = data => {
  return{
    type: types.SET_TEMPERATURE,
    payload: data
  };
};

export const setRealTemperature = data => {
  return{
    type: types.SET_REAL_TEMPERATURE,
    payload: data
  };
};

export const setResult = data => {
  return{
    type: types.SET_RESULT,
    payload: data
  };
};

