import axios from 'axios';

const apiKey = process.env.REACT_APP_API_KEY || '';
const baseUrl = 'http://api.openweathermap.org/data/2.5';


axios.interceptors.request.use(function (config) {
    config.baseURL = baseUrl;
    return config;
});

export const getCityWeatherApi = (data) => {
    console.log(data);
    // const {city} = data;
    return axios.get(`/weather?q=${data}&appid=${apiKey}&units=metric`)
};

