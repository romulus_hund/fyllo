import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import {applyMiddleware, compose, createStore} from 'redux';
import reducers from './store/reducers';
import {watchSaga} from './store/saga';
import 'antd/dist/antd.css';
import {BrowserRouter} from 'react-router-dom';
import App from './App';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose,
    sagaMiddleware = createSagaMiddleware(),
    middlewares = [sagaMiddleware];

export const store = createStore(reducers, composeEnhancers(applyMiddleware(...middlewares)));

sagaMiddleware.run(watchSaga);

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'));

