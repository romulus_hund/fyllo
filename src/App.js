import React, {useEffect} from 'react';
import './App.scss';
import {connect} from 'react-redux';
import {getCityWeather, setCity, setResult, setTemperature} from './store/actions/weatherActions';
import {Button, Input, Select} from 'antd';
import {getRealTemperature, getResults, getSelectedCity, getSelectedTemperature, isLoading} from './store/selectors';

const {Option} = Select;

const Result = ({selectedTemperature, realTemperature, isWin}) => {
    return (
        <div className={`${isWin ? 'win' : 'loose'} result-wrapper`}>
            <div className='selectedTemperature'>Real Temperature: {realTemperature}</div>
            <div className='realTemperature'>Your guess: {selectedTemperature}</div>
        </div>
    );
};

function App({getCityWeather, selectedCity, setCity, selectedTemperature, setTemperature, realTemperature, setResult, results}) {

    const compare = () => {
        const parsedSelectedTemperature = parseInt(selectedTemperature);

        let res = {
            selectedTemperature,
            realTemperature,
        };

        let diff = Math.abs(parsedSelectedTemperature) - Math.abs(realTemperature);

        if ((Math.abs(diff) > 5)) {
            res = {...res, isWin: false};

        } else {
            res = {...res, isWin: true};
        }

        setResult(res);
    };

    useEffect(()=> {
        if(realTemperature){
            compare();
        }

    }, [realTemperature]);

    useEffect(()=> {
        if(results.length === 5){
            const counter =  results.filter(res => res.isWin).length;

            if(counter > 2){
                alert('You win!');
            }else {
                alert('You loose!');
            }
        }
    }, [results]);

    const cities = ['London', 'Moscow', 'Berlin', 'Paris', 'New York'];

    const handleChange = (value) => {
        setCity(value);
    };

    const handleOnTempChange = e => {
        setTemperature(e.target.value);
    };

    const handleOnClick = () => {
        getCityWeather(selectedCity);
    };

    return (
        <div className="App">
            <div className='cities-wrapper'>
                <Select placeholder='Select city' style={{width: 120}} onChange={handleChange}>
                    {
                        cities.map((city, i) => <Option selected={i === 0} key={i} value={city}> {city}</Option>)
                    }
                </Select>
                <Input type='number' onChange={handleOnTempChange} value={selectedTemperature}
                       placeholder='Temperature'/>
            </div>

            <Button onClick={handleOnClick}>Check</Button>

            {
                results.length > 0 &&
                    results.map((res, i) => <Result isWin={res.isWin} key={i} realTemperature={res.realTemperature} selectedTemperature={res.selectedTemperature}/>)

            }
        </div>
    );
}

const mapStateToProps = state => {
    return {
        isLoading: isLoading(state),
        selectedCity: getSelectedCity(state),
        selectedTemperature: getSelectedTemperature(state),
        realTemperature: getRealTemperature(state),
        results: getResults(state),
    };
};

function mapDispatchToProps(dispatch) {
    return {
        getCityWeather: data => dispatch(getCityWeather(data)),
        setCity: data => dispatch(setCity(data)),
        setTemperature: data => dispatch(setTemperature(data)),
        setResult: data => dispatch(setResult(data)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
